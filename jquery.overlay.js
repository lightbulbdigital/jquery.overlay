
/*! Check if a jQuery object is, or contains, a DOM element or selector */
jQuery.fn.isOrHas = function( other ){
    return this.is(other) || !!this.has(other).length;
};

/*! jQuery Overlay plugin https://bitbucket.org/lightbulbdigital/jquery.overlay */
jQuery.fn.overlay = function( args ){
    'use strict';

    var $       = jQuery;
    var $this   = this;

    // The default options can be overwritting by passing in args
    var options = $.extend({

        openClass       : 'is-active'   ,
        classTarget     : $this         ,

        openButton      : ''            ,
        closeButton     : ''            ,
        toggleButton    : ''            ,

        clickOutToClose : true          ,
        escToClose      : true          ,

        eventPrefix     : 'overlay'     ,
        openEvent       : null          ,
        closeEvent      : null          ,
        toggleEvent     : null          ,
        changeEvent     : null

    }, args);

    // Jqueryify any raw elements or selectors
    if( !(options.classTarget   instanceof $) ){ options.classTarget    = $( options.classTarget    ); }
    if( !(options.openButton    instanceof $) ){ options.openButton     = $( options.openButton     ); }
    if( !(options.closeButton   instanceof $) ){ options.closeButton    = $( options.closeButton    ); }
    if( !(options.toggleButton  instanceof $) ){ options.toggleButton   = $( options.toggleButton   ); }

    // Setup event names
    options.openEvent   = options.openEvent     || options.eventPrefix + 'Open'     ;
    options.closeEvent  = options.closeEvent    || options.eventPrefix + 'Close'    ;
    options.toggleEvent = options.toggleEvent   || options.eventPrefix + 'Toggle'   ;
    options.changeEvent = options.changeEvent   || options.eventPrefix + 'Change'   ;

    // Open & close event handlers
    $this.on( options.openEvent , function(e){
        if( !$this.is(e.target) ){ return; }
        if( options.classTarget.hasClass(options.openClass) ){ return; }
        options.classTarget.addClass( options.openClass );
        $this.trigger( options.changeEvent );
    });
    $this.on( options.closeEvent, function(e){
        if( !$this.is(e.target) ){ return; }
        if( !options.classTarget.hasClass(options.openClass) ){ return; }
        options.classTarget.removeClass( options.openClass );
        $this.trigger( options.changeEvent );
    });

    // The toggle event handler simply triggers the relevant open/close event
    $this.on( options.toggleEvent, function(e){
        if( !$this.is(e.target) ){ return; }
        if( options.classTarget.hasClass(options.openClass) ){
            $this.trigger( options.closeEvent );
        }else{
            $this.trigger( options.openEvent );
        }
    });

    // Make each button trigger the appropriate event
    options.openButton  .on( 'click', function(){ $this.trigger( options.openEvent      ); } );
    options.closeButton .on( 'click', function(){ $this.trigger( options.closeEvent     ); } );
    options.toggleButton.on( 'click', function(){ $this.trigger( options.toggleEvent    ); } );

    // Click off the overlay to close
    if( options.clickOutToClose ){
        $(document).on( 'click', function(e){
            if( options.openButton      .isOrHas(e.target)  ){ return; } // Ignore clicks on the open button
            if( options.closeButton     .isOrHas(e.target)  ){ return; } // Ignore clicks on the close button
            if( options.toggleButton    .isOrHas(e.target)  ){ return; } // Ignore clicks on the toggle button
            if( $this                   .isOrHas(e.target)  ){ return; } // Ignore clicks on the overlay itself

            $this.trigger( options.closeEvent ); // All other clicks close the overlay
        } );
    }

    // Press Esc to close
    if( options.escToClose ){
        $(document).on( 'keydown', function( e ){
            if( e.key == "Escape" || e.key == "Esc" || e.keyCode == 27 ){
                $this.trigger( options.closeEvent );
            }
        });
    }

    // Chainable
    return this;
};
