# Simple Jquery Overylay

# Install
Using the power of NPM

```
npm i bitbucket:lightbulbdigital/jquery.overlay
```

# Use
Wield the ancient power of jQuery

```javascript
$('#menu').overlay({
    openButton: '#open-menu',
    classTarget: 'body'
});
```

# Options & example
See `README.html`